module github.com/poundbot/poundbot

require (
	bitbucket.org/mrpoundsign/poundbot v0.2.2 // indirect
	github.com/benbjohnson/clock v0.0.0-20161215174838-7dc76406b6d3
	github.com/blang/semver v3.5.1+incompatible
	github.com/bwmarrin/discordgo v0.19.0
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gorilla/mux v1.7.1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pelletier/go-toml v1.3.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.3.2
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190404164418-38d8ce5564a5 // indirect
	golang.org/x/sys v0.0.0-20190410235845-0ad05ae3009d // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
